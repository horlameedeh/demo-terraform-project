variable "aws_region" {
  type    = string
  default = "eu-central-1"
}

variable "vpc_name" {
  type    = string
  default = "demo_vpc"
}

variable "vpc_cidr" {
  type    = string
  default = "10.0.0.0/16"
}

variable "private_subnets" {
  type = map(number)
  default = {
    "eu-central-1a" = 1
    "eu-central-1b" = 2
    "eu-central-1c" = 3
  }
}

variable "public_subnets" {
  type = map(number)
  default = {
    "eu-central-1a" = 1
    "eu-central-1b" = 2
    "eu-central-1c" = 3
  }
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "my_ip" {
  default = "92.117.171.115/32"
}



